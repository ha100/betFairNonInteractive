//
//  Networking.m
//  betFairNonInteractive
//
//  Created by tom Hastik on 23/04/16.
//  Copyright © 2016 ha100. All rights reserved.
//

#import "Networking.h"
#import "NSURL+Additions.h"
#import "BETFAIR_MutableURLRequest.h"

NSString *const certificatePassword = @""; // EDIT CERTIFICATE PASSWORD

typedef NSURLSessionAuthChallengeDisposition (^AuthenticationChallengeBlock)(NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential * __autoreleasing *credential);

@interface Networking ()
    @property (nonatomic, strong) NSURLSession *session;
    @property (nonatomic, strong) NSURLCredential *certCredential;
@end

@implementation Networking

- (instancetype)init {
    
    if (self = [super init]) {

        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

        NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
        [operationQueue setMaxConcurrentOperationCount:1];

        [self setSession:[NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:operationQueue]];
    }
    
    return self;
}

- (void)sendPacket:(PacketList)packet
        withObject:(id _Nullable)object
        completion:(void (^ _Nonnull)(id response , NSError * _Nullable error ))completion {

    NSURL *url;

    switch (packet) {
        case LoginToAccount:
            url = [NSURL BETFAIR_loginURL];
            break;
        case GetAccountDetails:
            url = [NSURL BETFAIR_accountURL:BETFAIR_AccountOperation.getAccountDetails];
            break;
        default:
            break;
    }
    
    NSMutableURLRequest *request = [[BETFAIR_MutableURLRequest alloc] initWithURL:url andSession:[self cookie]];

    NSURLSessionDataTask *dataTask = [[self session] dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
                if (!error && data) {

                    NSError *error;
                    
                    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                
                    if (completion) {
                        if (!error) {
                            completion(result, error);
                        } else {
                            completion([[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding], error);
                        }
                    }
                } else {
                    if (completion)
                        completion(data, error);
                }
            }];
    
    [dataTask resume];
}

- (void)URLSession:(NSURLSession *)session
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * __nullable credential))completionHandler {
    completionHandler(NSURLSessionAuthChallengeUseCredential, [self prepareCredential]);
}

#pragma mark SSL certificates

- (NSURLCredential *)prepareCredential {
    
    if (![self certCredential]) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"client-2048" ofType:@"pkcs12"];
        
        NSAssert(path != nil, @"Specified certificate does not exist!");
        
        NSData *p12data = [NSData dataWithContentsOfFile:path];
        CFDataRef inP12data = (__bridge CFDataRef)p12data;
        
        SecIdentityRef myIdentity;
        SecTrustRef myTrust;
        extractIdentityAndTrust(inP12data, &myIdentity, &myTrust);
        
        SecCertificateRef myCertificate;
        SecIdentityCopyCertificate(myIdentity, &myCertificate);
        
        NSAssert(myCertificate != NULL, @"Failed to create certificate object. Is the certificate in PKCS12 format?");
        
        const void *certs[] = { myCertificate };
        CFArrayRef certsArray = CFArrayCreate(NULL, certs, 1, NULL);
        
        [self setCertCredential:[NSURLCredential credentialWithIdentity:myIdentity certificates:(__bridge NSArray *)certsArray persistence:NSURLCredentialPersistencePermanent]];
    }
    
    return [self certCredential];
}

OSStatus extractIdentityAndTrust(CFDataRef inP12data, SecIdentityRef *identity, SecTrustRef *trust) {
    OSStatus securityError = errSecSuccess;
    
    const void *keys[] = { kSecImportExportPassphrase };
    const void *values[] = { (__bridge CFStringRef)certificatePassword };
    
    CFDictionaryRef options = CFDictionaryCreate(NULL, keys, values, 1, NULL, NULL);
    
    CFArrayRef items = CFArrayCreate(NULL, 0, 0, NULL);
    
    securityError = SecPKCS12Import(inP12data, options, &items);
    
    if (securityError == 0) {
        CFDictionaryRef myIdentityAndTrust = CFArrayGetValueAtIndex(items, 0);
        const void *tempIdentity = NULL;
        tempIdentity = CFDictionaryGetValue(myIdentityAndTrust, kSecImportItemIdentity);
        *identity = (SecIdentityRef)tempIdentity;
        const void *tempTrust = NULL;
        tempTrust = CFDictionaryGetValue(myIdentityAndTrust, kSecImportItemTrust);
        *trust = (SecTrustRef)tempTrust;
    }
    
    if (options) {
        CFRelease(options);
    }
    
    return securityError;
}

@end
