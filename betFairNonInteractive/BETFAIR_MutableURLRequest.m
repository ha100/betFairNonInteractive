//
//  BETFAIR_MutableURLRequest.m
//  betFairNonInteractive
//
//  Created by tom Hastik on 23/04/16.
//  Copyright © 2016 ha100. All rights reserved.
//

#import "BETFAIR_MutableURLRequest.h"
#import "NSMutableURLRequest+Additions.h"

NSString *const BETFAIR_appKey                      = @"";  // EDIT BETFAIR APPKEY
NSString *const BETFAIR_product                     = @"";  // EDIT BETFAIR PRODUCT NAME
NSString *const BETFAIR_username                    = @"";  // EDIT BETFAIR USERNAME
NSString *const BETFAIR_password                    = @"";  // EDIT BETFAIR PASSWORD

NSString *const BETFAIR_scheme                      = @"bfexsc";

NSString *const BETFAIR_XApplication                = @"X-Application";
NSString *const BETFAIR_XAuthentication             = @"X-Authentication";
NSString *const BETFAIR_ContentType                 = @"Content-Type";
NSString *const BETFAIR_JsonContent                 = @"application/json";
NSString *const BETFAIR_LoginContent                = @"application/x-www-form-urlencoded";
NSString *const BETFAIR_Accept                      = @"Accept";
NSString *const BETFAIR_AcceptCharset               = @"Accept-Charset";
NSString *const BETFAIR_AcceptCharsetUtf            = @"UTF-8";

@implementation BETFAIR_MutableURLRequest

- (instancetype)initWithURL:(NSURL *)URL andSession:(NSString *)token {
    
    if (self = [super initWithURL:URL]) {

        [self setValue:BETFAIR_JsonContent forHTTPHeaderField:BETFAIR_Accept];
        [self setValue:BETFAIR_appKey forHTTPHeaderField:BETFAIR_XApplication];
        [self setValue:BETFAIR_AcceptCharsetUtf forHTTPHeaderField:BETFAIR_AcceptCharset];
        
        [self setHTTPMethod:@"POST"];
        
        if ([token length]) {
            [self setValue:BETFAIR_JsonContent forHTTPHeaderField:BETFAIR_ContentType];
            [self setValue:token forHTTPHeaderField:BETFAIR_XAuthentication];
        } else {
            [self setValue:[BETFAIR_scheme stringByAppendingString:@"://ios.betfair.com/login"] forHTTPHeaderField:@"url"];
            [self setValue:BETFAIR_product forHTTPHeaderField:@"applicationId"];
            [self setValue:BETFAIR_product forHTTPHeaderField:@"product"];
            [self setValue:BETFAIR_LoginContent forHTTPHeaderField:BETFAIR_ContentType];

            [self setValue:@"POST" forHTTPHeaderField:@"redirectMethod"];

            [self addBodyKey:@"username" value:BETFAIR_username];
            [self addBodyKey:@"password" value:BETFAIR_password];
        }
    }
    
    return self;
}

@end
