//
//  NSString+URLEncoding.h
//  betFairNonInteractive
//
//  Created by tom Hastik on 23/04/16.
//  Copyright © 2016 ha100. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

@property (nonatomic, readonly) NSString *urlEncodedString;
@property (nonatomic, readonly) NSString *urlDecodedString;

@end
