//
//  Networking.h
//  betFairNonInteractive
//
//  Created by tom Hastik on 23/04/16.
//  Copyright © 2016 ha100. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PACKET_STRING(enum) [@[ @"LoginToAccount", \
                                @"GetAccountDetails"] objectAtIndex:enum]

typedef enum {
    LoginToAccount,
    GetAccountDetails
} PacketList;

@interface Networking : NSObject <NSURLSessionDelegate>

@property (nonatomic, strong, nullable) NSString *cookie;

- (void)sendPacket:(PacketList)packet
        withObject:(id _Nullable)object
        completion:(void (^ _Nonnull)(id _Nullable response , NSError * _Nullable error ))completion;

@end
