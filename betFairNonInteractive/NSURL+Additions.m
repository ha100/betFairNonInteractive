//
//  NSURL+Additions.m
//  betFairNonInteractive
//
//  Created by tom Hastik on 23/04/16.
//  Copyright © 2016 ha100. All rights reserved.
//

#import "NSURL+Additions.h"

const struct BETFAIR_AccountOperation BETFAIR_AccountOperation = {
    .getAccountDetails = @"getAccountDetails",
};

NSString *const BETFAIR_baseUrl             = @"https://api.betfair.com/exchange";
NSString *const BETFAIR_baseLoginUrl        = @"https://identitysso.betfair.com";
NSString *const BETFAIR_loginOperation      = @"certlogin";
NSString *const BETFAIR_apiVersion          = @"1.0";
NSString *const BETFAIR_heartbeatVersion    = @"1";

@implementation NSURL (Additions)

+ (NSURL *)BETFAIR_loginURL {
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/%@", BETFAIR_baseLoginUrl, BETFAIR_loginOperation]];
}

+ (NSURL *)BETFAIR_accountURL:(NSString *)operation {

    NSParameterAssert([operation length]);
    
    if ([operation length]) {
        return [NSURL URLWithString:[NSString stringWithFormat:@"%@/account/rest/v%@/%@/", BETFAIR_baseUrl, BETFAIR_apiVersion, operation]];
    } else {
        return nil;
    }
}

@end
