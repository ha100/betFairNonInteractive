//
//  BETFAIR_MutableURLRequest.h
//  betFairNonInteractive
//
//  Created by tom Hastik on 23/04/16.
//  Copyright © 2016 ha100. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BETFAIR_MutableURLRequest : NSMutableURLRequest

- (instancetype)initWithURL:(NSURL *)URL andSession:(NSString *)token;

@end
