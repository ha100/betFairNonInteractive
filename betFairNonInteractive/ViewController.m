//
//  ViewController.m
//  betFairNonInteractive
//
//  Created by tom Hastik on 23/04/16.
//  Copyright © 2016 ha100. All rights reserved.
//

#import "ViewController.h"
#import "Networking.h"

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [[self view] setBackgroundColor:[UIColor blackColor]];
    
    Networking *networking = [[Networking alloc] init];
    
    [networking sendPacket:LoginToAccount withObject:nil completion:^(id response, NSError *error) {
        
        if ([[response objectForKey:@"loginStatus"] isEqualToString:@"SUCCESS"]) {
            
            [networking setCookie:[response objectForKey:@"sessionToken"]];
            
                        [networking sendPacket:GetAccountDetails withObject:nil completion:^(NSDictionary *response, NSError *error) {
                            NSLog(@"got %@", response);
                        }];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
