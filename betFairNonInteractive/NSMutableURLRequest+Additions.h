//
//  NSMutableURLRequest+Additions.h
//  betFairNonInteractive
//
//  Created by tom Hastik on 23/04/16.
//  Copyright © 2016 ha100. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableURLRequest (Additions)

- (void)addBodyKey:(NSString *)key value:(NSString *)value;

@end
