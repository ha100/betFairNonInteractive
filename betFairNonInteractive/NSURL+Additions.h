//
//  NSURL+Additions.h
//  betFairNonInteractive
//
//  Created by tom Hastik on 23/04/16.
//  Copyright © 2016 ha100. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const struct BETFAIR_AccountOperation {
    __unsafe_unretained NSString *getAccountDetails;
} BETFAIR_AccountOperation;

@interface NSURL (Additions)

+ (NSURL *)BETFAIR_loginURL;
+ (NSURL *)BETFAIR_accountURL:(NSString *)operation;

@end
