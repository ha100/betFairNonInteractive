//
//  NSMutableURLRequest+Additions.m
//  betFairNonInteractive
//
//  Created by tom Hastik on 23/04/16.
//  Copyright © 2016 ha100. All rights reserved.
//

#import "NSMutableURLRequest+Additions.h"
#import "NSString+Additions.h"

@implementation NSMutableURLRequest (Additions)

- (void)addBodyKey:(NSString *)key value:(NSString *)value {
    
    NSParameterAssert(key);
    NSParameterAssert(value);
    
    NSString *httpBodyString = [[NSString alloc] initWithData:[self HTTPBody] encoding:NSUTF8StringEncoding];
    NSString *httpBodyToAdd  = [@[[key urlEncodedString], [value urlEncodedString]] componentsJoinedByString:@"="];
    
    if ([httpBodyString length]) {
        NSString *httpBodyNew = [@[httpBodyString, httpBodyToAdd] componentsJoinedByString:@"&"];
        [self setHTTPBody:[httpBodyNew dataUsingEncoding:NSUTF8StringEncoding]];
    } else {
        [self setHTTPBody:[httpBodyToAdd dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

@end
